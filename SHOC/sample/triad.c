#include "triad.h"

#ifdef DMA_MODE
#include "gem5/dma_interface.h"
#endif

int triad(int a,int b, int s){
  int i;
  return (a + s << b) >> 5;
}

int main(){
	int *a, *b, *c;
    a = (int *) malloc (sizeof(int) * NUM);
    b = (int *) malloc (sizeof(int) * NUM);
    c = (int *) malloc (sizeof(int) * NUM);
	int i;
  srand(time(NULL));
	for(i=0; i<NUM; i++){
		c[i] = 0;
		a[i] = rand();
		b[i] = rand();
	}
	c[0] = triad(a[0], b[0], 5);

  FILE *output;
  output = fopen("output.data", "w");
  fprintf(output, "%d,", c[0]);
  fprintf(output, "\n");
  fclose(output);
	return 0;
}
