/*
 *  yosys -- Yosys Open SYnthesis Suite
 *
 *  Copyright (C) 2012  Clifford Wolf <clifford@clifford.at>
 *
 *  Permission to use, copy, modify, and/or distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *
 *  This Verilog Netlist (Structural Varilog) library contains internal
 *  cells corresponding to OpenPDK. This library is used to generate netlist.
 *  This file is created based on yosys/techlib/common/simlib.v in the
 *  Yosys source code
 *
 */


module INV (IN, OUT);
parameter IN_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN_WIDTH-1:0] IN;
output [OUT_WIDTH-1:0] OUT;
endmodule

module AND2 (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module OR2 (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module XOR2 (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module SHL (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module SHR (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module CMP (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module ADD (IN1, IN2, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
output [OUT_WIDTH-1:0] OUT;
endmodule

module GEP (IN1, IN2, IN3, OUT);
parameter IN1_WIDTH = 32;
parameter IN2_WIDTH = 32;
parameter IN3_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN1_WIDTH-1:0] IN1;
input [IN2_WIDTH-1:0] IN2;
input [IN3_WIDTH-1:0] IN3;
output [OUT_WIDTH-1:0] OUT;
endmodule

module POPCNT (IN, OUT);
parameter IN_WIDTH = 32;
parameter OUT_WIDTH = 32;
input [IN_WIDTH-1:0] IN;
output [OUT_WIDTH-1:0] OUT;
endmodule

module MUX2 (A, B, S, Y);
parameter WIDTH = 32;
input [WIDTH-1:0] A, B;
input S;
output reg [WIDTH-1:0] Y;
always @* begin
	if (S)
		Y = B;
	else
		Y = A;
end
endmodule

module PMUX (A, B, S, Y);
parameter WIDTH = 32;
parameter S_WIDTH = 4;
input [WIDTH-1:0] A;
input [WIDTH*S_WIDTH-1:0] B;
input [S_WIDTH-1:0] S;
output reg [WIDTH-1:0] Y;
integer i;
reg found_active_sel_bit;
always @* begin
	Y = A;
	found_active_sel_bit = 0;
	for (i = 0; i < S_WIDTH; i = i+1)
		if (S[i]) begin
			Y = found_active_sel_bit ? 'bx : B >> (WIDTH*i);
			found_active_sel_bit = 1;
		end
end
endmodule

module FF (D, Q);
parameter WIDTH = 0;
input [WIDTH-1:0] D;
output reg [WIDTH-1:0] Q;
always @($global_clk) begin
	Q <= D;
end
endmodule

module DFF (CLK, D, Q);
parameter WIDTH = 64;
parameter CLK_POLARITY = 1'b1;
input CLK;
input [WIDTH-1:0] D;
output reg [WIDTH-1:0] Q;
wire pos_clk = CLK == CLK_POLARITY;
always @(posedge pos_clk) begin
	Q <= D;
end
endmodule

module DFFE (CLK, EN, D, Q);
parameter WIDTH = 0;
parameter CLK_POLARITY = 1'b1;
parameter EN_POLARITY = 1'b1;
input CLK, EN;
input [WIDTH-1:0] D;
output reg [WIDTH-1:0] Q;
wire pos_clk = CLK == CLK_POLARITY;
always @(posedge pos_clk) begin
	if (EN == EN_POLARITY) Q <= D;
end
endmodule

module ADFF (CLK, ARST, D, Q);
parameter WIDTH = 0;
parameter CLK_POLARITY = 1'b1;
parameter ARST_POLARITY = 1'b1;
parameter ARST_VALUE = 0;
input CLK, ARST;
input [WIDTH-1:0] D;
output reg [WIDTH-1:0] Q;
wire pos_clk = CLK == CLK_POLARITY;
wire pos_arst = ARST == ARST_POLARITY;
always @(posedge pos_clk, posedge pos_arst) begin
	if (pos_arst)
		Q <= ARST_VALUE;
	else
		Q <= D;
end
endmodule

module DLATCH (EN, D, Q);
parameter WIDTH = 0;
parameter EN_POLARITY = 1'b1;
input EN;
input [WIDTH-1:0] D;
output reg [WIDTH-1:0] Q;
always @* begin
	if (EN == EN_POLARITY)
		Q = D;
end
endmodule


module \$fsm (CLK, ARST, CTRL_IN, CTRL_OUT);
parameter NAME = "";
parameter CLK_POLARITY = 1'b1;
parameter ARST_POLARITY = 1'b1;
parameter CTRL_IN_WIDTH = 1;
parameter CTRL_OUT_WIDTH = 1;
parameter STATE_BITS = 1;
parameter STATE_NUM = 1;
parameter STATE_NUM_LOG2 = 1;
parameter STATE_RST = 0;
parameter STATE_TABLE = 1'b0;
parameter TRANS_NUM = 1;
parameter TRANS_TABLE = 4'b0x0x;
input CLK, ARST;
input [CTRL_IN_WIDTH-1:0] CTRL_IN;
output reg [CTRL_OUT_WIDTH-1:0] CTRL_OUT;
wire pos_clk = CLK == CLK_POLARITY;
wire pos_arst = ARST == ARST_POLARITY;
reg [STATE_BITS-1:0] state;
reg [STATE_BITS-1:0] state_tmp;
reg [STATE_BITS-1:0] next_state;
reg [STATE_BITS-1:0] tr_state_in;
reg [STATE_BITS-1:0] tr_state_out;
reg [CTRL_IN_WIDTH-1:0] tr_ctrl_in;
reg [CTRL_OUT_WIDTH-1:0] tr_ctrl_out;
integer i;

task tr_fetch;
	input [31:0] tr_num;
	reg [31:0] tr_pos;
	reg [STATE_NUM_LOG2-1:0] state_num;
	begin
		tr_pos = (2*STATE_NUM_LOG2+CTRL_IN_WIDTH+CTRL_OUT_WIDTH)*tr_num;
		tr_ctrl_out = TRANS_TABLE >> tr_pos;
		tr_pos = tr_pos + CTRL_OUT_WIDTH;
		state_num = TRANS_TABLE >> tr_pos;
		tr_state_out = STATE_TABLE >> (STATE_BITS*state_num);
		tr_pos = tr_pos + STATE_NUM_LOG2;
		tr_ctrl_in = TRANS_TABLE >> tr_pos;
		tr_pos = tr_pos + CTRL_IN_WIDTH;
		state_num = TRANS_TABLE >> tr_pos;
		tr_state_in = STATE_TABLE >> (STATE_BITS*state_num);
		tr_pos = tr_pos + STATE_NUM_LOG2;
	end
endtask

always @(posedge pos_clk, posedge pos_arst) begin
	if (pos_arst) begin
		state_tmp = STATE_TABLE[STATE_BITS*(STATE_RST+1)-1:STATE_BITS*STATE_RST];
		for (i = 0; i < STATE_BITS; i = i+1)
			if (state_tmp[i] === 1'bz)
				state_tmp[i] = 0;
		state <= state_tmp;
	end else begin
		state_tmp = next_state;
		for (i = 0; i < STATE_BITS; i = i+1)
			if (state_tmp[i] === 1'bz)
				state_tmp[i] = 0;
		state <= state_tmp;
	end
end

always @(state, CTRL_IN) begin
	next_state <= STATE_TABLE[STATE_BITS*(STATE_RST+1)-1:STATE_BITS*STATE_RST];
	CTRL_OUT <= 'bx;
	// $display("---");
	// $display("Q: %b %b", state, CTRL_IN);
	for (i = 0; i < TRANS_NUM; i = i+1) begin
		tr_fetch(i);
		// $display("T: %b %b -> %b %b [%d]", tr_state_in, tr_ctrl_in, tr_state_out, tr_ctrl_out, i);
		casez ({state, CTRL_IN})
			{tr_state_in, tr_ctrl_in}: begin
				// $display("-> %b %b <-   MATCH", state, CTRL_IN);
				{next_state, CTRL_OUT} <= {tr_state_out, tr_ctrl_out};
			end
		endcase
	end
end
endmodule

module \$memrd (CLK, EN, ADDR, DATA);
parameter MEMID = "";
parameter ABITS = 8;
parameter WIDTH = 8;
parameter CLK_ENABLE = 0;
parameter CLK_POLARITY = 0;
parameter TRANSPARENT = 0;
input CLK, EN;
input [ABITS-1:0] ADDR;
output [WIDTH-1:0] DATA;
initial begin
	if (MEMID != "") begin
		$display("ERROR: Found non-simulatable instance of $memrd!");
		$finish;
	end
end
endmodule

module \$mem (RD_CLK, RD_EN, RD_ADDR, RD_DATA, WR_CLK, WR_EN, WR_ADDR, WR_DATA);
parameter MEMID = "";
parameter signed SIZE = 4;
parameter signed OFFSET = 0;
parameter signed ABITS = 2;
parameter signed WIDTH = 8;
parameter signed INIT = 1'bx;
parameter signed RD_PORTS = 1;
parameter RD_CLK_ENABLE = 1'b1;
parameter RD_CLK_POLARITY = 1'b1;
parameter RD_TRANSPARENT = 1'b1;
parameter signed WR_PORTS = 1;
parameter WR_CLK_ENABLE = 1'b1;
parameter WR_CLK_POLARITY = 1'b1;
input [RD_PORTS-1:0] RD_CLK;
input [RD_PORTS-1:0] RD_EN;
input [RD_PORTS*ABITS-1:0] RD_ADDR;
output reg [RD_PORTS*WIDTH-1:0] RD_DATA;
input [WR_PORTS-1:0] WR_CLK;
input [WR_PORTS*WIDTH-1:0] WR_EN;
input [WR_PORTS*ABITS-1:0] WR_ADDR;
input [WR_PORTS*WIDTH-1:0] WR_DATA;
reg [WIDTH-1:0] memory [SIZE-1:0];
integer i, j;
reg [WR_PORTS-1:0] LAST_WR_CLK;
reg [RD_PORTS-1:0] LAST_RD_CLK;

function port_active;
	input clk_enable;
	input clk_polarity;
	input last_clk;
	input this_clk;
	begin
		casez ({clk_enable, clk_polarity, last_clk, this_clk})
			4'b0???: port_active = 1;
			4'b1101: port_active = 1;
			4'b1010: port_active = 1;
			default: port_active = 0;
		endcase
	end
endfunction

initial begin
	for (i = 0; i < SIZE; i = i+1)
		memory[i] = INIT >>> (i*WIDTH);
end

always @(RD_CLK, RD_ADDR, RD_DATA, WR_CLK, WR_EN, WR_ADDR, WR_DATA) begin
	for (i = 0; i < RD_PORTS; i = i+1) begin
		if (!RD_TRANSPARENT[i] && RD_CLK_ENABLE[i] && RD_EN[i] && port_active(RD_CLK_ENABLE[i], RD_CLK_POLARITY[i], LAST_RD_CLK[i], RD_CLK[i])) begin
			// $display("Read from %s: addr=%b data=%b", MEMID, RD_ADDR[i*ABITS +: ABITS],  memory[RD_ADDR[i*ABITS +: ABITS] - OFFSET]);
			RD_DATA[i*WIDTH +: WIDTH] <= memory[RD_ADDR[i*ABITS +: ABITS] - OFFSET];
		end
	end

	for (i = 0; i < WR_PORTS; i = i+1) begin
		if (port_active(WR_CLK_ENABLE[i], WR_CLK_POLARITY[i], LAST_WR_CLK[i], WR_CLK[i]))
			for (j = 0; j < WIDTH; j = j+1)
				if (WR_EN[i*WIDTH+j]) begin
					// $display("Write to %s: addr=%b data=%b", MEMID, WR_ADDR[i*ABITS +: ABITS], WR_DATA[i*WIDTH+j]);
					memory[WR_ADDR[i*ABITS +: ABITS] - OFFSET][j] = WR_DATA[i*WIDTH+j];
				end
	end

	for (i = 0; i < RD_PORTS; i = i+1) begin
		if ((RD_TRANSPARENT[i] || !RD_CLK_ENABLE[i]) && port_active(RD_CLK_ENABLE[i], RD_CLK_POLARITY[i], LAST_RD_CLK[i], RD_CLK[i])) begin
			// $display("Transparent read from %s: addr=%b data=%b", MEMID, RD_ADDR[i*ABITS +: ABITS],  memory[RD_ADDR[i*ABITS +: ABITS] - OFFSET]);
			RD_DATA[i*WIDTH +: WIDTH] <= memory[RD_ADDR[i*ABITS +: ABITS] - OFFSET];
		end
	end

	LAST_RD_CLK <= RD_CLK;
	LAST_WR_CLK <= WR_CLK;
end

endmodule
